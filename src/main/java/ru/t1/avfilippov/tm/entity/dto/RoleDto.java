package ru.t1.avfilippov.tm.entity.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.avfilippov.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "tm.tm_role")
public class RoleDto {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String userId;

    @Column
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

}
