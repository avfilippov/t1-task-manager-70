package ru.t1.avfilippov.tm.util;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.t1.avfilippov.tm.entity.dto.CustomUser;


public final class UserUtil {

    private UserUtil() {
    }

    public static String getUserId() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principial = authentication.getPrincipal();
        if (principial == null) throw new AccessDeniedException("");
        if (!(principial instanceof CustomUser)) throw new AccessDeniedException("");
        final CustomUser customUser = (CustomUser) principial;
        return customUser.getUserId();
    }

}
