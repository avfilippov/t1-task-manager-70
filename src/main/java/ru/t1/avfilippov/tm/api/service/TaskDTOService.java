package ru.t1.avfilippov.tm.api.service;

import ru.t1.avfilippov.tm.entity.dto.TaskDto;

import java.util.List;

public interface TaskDTOService {

    List<TaskDto> findAll(String userId);

    TaskDto save(TaskDto task);

    TaskDto save(String userId);

    TaskDto findById(String id);

    boolean existsById(String id);

    TaskDto findByUserIdAndId(String userId, String id);

    boolean existsByUserIdAndId(String userId, String id);

    void deleteByUserIdAndId(String userId, String id);

    long countByUserId(String userId);

    void deleteById(String id);

    void delete(TaskDto task);

    void deleteAll(List<TaskDto> tasks);

    void clear(String userId);

}
