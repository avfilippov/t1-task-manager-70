package ru.t1.avfilippov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.avfilippov.tm.api.service.ProjectDTOService;
import ru.t1.avfilippov.tm.entity.dto.ProjectDto;
import ru.t1.avfilippov.tm.repository.ProjectDtoRepository;
import ru.t1.avfilippov.tm.util.UserUtil;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ProjectDtoServiceImpl implements ProjectDTOService {

    @Autowired
    private ProjectDtoRepository projectRepository;

    @Override
    public List<ProjectDto> findAll(final String userId) {
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public ProjectDto save(final ProjectDto project) {
        return projectRepository.save(project);
    }

    @Override
    public ProjectDto save(final String userId) {
        final ProjectDto project = new ProjectDto("New Project " + LocalDateTime.now().toString());
        project.setUserId(UserUtil.getUserId());
        return projectRepository.save(project);
    }

    @Override
    public ProjectDto findById(final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    public boolean existsById(final String id) {
        return projectRepository.existsById(id);
    }

    @Override
    public long countByUserId(final String userId) {
        return projectRepository.countByUserId(userId);
    }

    @Override
    public void deleteById(final String id) {
        projectRepository.deleteById(id);
    }

    @Override
    public void delete(final ProjectDto project) {
        projectRepository.delete(project);
    }

    @Override
    public void deleteAll(final List<ProjectDto> projects) {
        projectRepository.deleteAll(projects);
    }

    @Override
    public void clear(final String userId) {
        projectRepository.deleteByUserId(userId);
    }

    @Override
    public ProjectDto findByUserIdAndId(final String userId, final String id) {
        return projectRepository.findByUserIdAndId(userId, id).orElse(null);
    }

    @Override
    public boolean existsByUserIdAndId(final String userId, final String id) {
        return projectRepository.findByUserIdAndId(userId, id).isPresent();
    }

    @Override
    public void deleteByUserIdAndId(final String userId, final String id) {
        projectRepository.deleteByUserIdAndId(userId, id);
    }

}
